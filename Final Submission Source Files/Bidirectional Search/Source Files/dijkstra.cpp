#include "min_heap.h"
#include "utils.h"
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>

using namespace std;

UMAP <int, UMAP <int, int> > adjacency_list;
UMAP<int, pair<int, int> > locations;

string ADJ_FILE_PREFIX;

int dijkstra(int src, int dest) {
	hash_table node_dist, prev;
	min_heap heap(node_dist);

	node_dist[src] = 0;
	prev[src] = -1;
	heap.push(src);
	int iters = 0;

	struct timeval start, end;   /* gettimeofday stuff */
	struct timezone tzp;

	gettimeofday(&start, &tzp);
	while (heap.get_size()) {
		iters += 1;

		int node = heap.extract_min();
		if (node == dest)
			break;

		for (auto nei : adjacency_list[node]) {
			int dist = node_dist[node] + nei.second;
			if (node_dist.find(nei.first) == node_dist.end()
					|| dist < node_dist[nei.first]) {
				node_dist[nei.first] = dist;
				prev[nei.first] = node;
				heap.decrement_key(nei.first);
			}
		}
	}

	gettimeofday(&end, &tzp);
	long elapsed_usecs = get_elapsed(&start, &end);

	cout << OUT_DEL << "SEQ " << 1 << " " << abs(node_dist[dest]) << " " <<
				iters << " " << elapsed_usecs << " 0 " << elapsed_usecs << endl;

	return node_dist[dest];
}

int main() {
    cin >> ADJ_FILE_PREFIX;
	load_adjacency_list(adjacency_list, ADJ_FILE_PREFIX, 0, 1);

	int T, src, dest;
	cin >> T;
	for (int i = 0; i < T; i += 1) {
		cin >> src >> dest;
		dijkstra(src, dest);
	}

	return 0;
}
