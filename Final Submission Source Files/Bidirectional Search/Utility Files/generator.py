'''
Created on May 2, 2015

@author: ga-capar
'''

from math import ceil

# Template to generate the job file
# Arguments: #nodes, #procs/node, #mem, job_name, command_to_run
template = '''#PBS -W x=NACCESSPOLICY:SINGLEJOB
#PBS -l nodes=%d:nehalem:ppn=%d
#PBS -l walltime=01:00:00
#PBS -l mem=%dgb
#PBS -N %s
#PBS -S /bin/bash
#PBS -j oe

source /g/home/gsunder/setup_env.sh

# $PBS_O_WORKDIR is the directory from which the job was submitted
cd $PBS_O_WORKDIR

# Command to run
%s
'''

threads = 1
while threads <= 128:
    with open('job_files/a_bidijkstra_%d' % (threads), 'w') as F:
        F.write(template % (ceil(threads/8.0), min(8, threads), threads, 'a_bidijkstra_openmp_%d' % (threads), 
                            'OMP_NUM_THREADS=%d ./a_bidijkstra < test_input' % (threads)))
        
    threads *= 2
    
