'''
Created on May 2, 2015
@author: ga-capar
'''

from collections import defaultdict
from numpy import mean, std
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import pylab
import glob

OUT_DELIMITER = 'METRICS'



# Process lines of format: 
# METRICS OMP 128 3668233 91 1569141 1645302 3214443
# DELIMITER TYPE PROCS DISTANCE ITERS PROC_TIME MSG_TIME TOT_TIME
class TimingInfo:
    PROC_TIME = 'PROC'
    MSG_TIME = 'MSG'
    TOTAL_TIME = 'TOTAL'
    
    DISTANCE = 'DISTANCE'
    
    OMP = 'OMP'
    SEQ = 'SEQ'
    MPI = 'MPI'
    OMP_A = 'OMP_A'
    SEQ_A = 'SEQ_A'
    
    def __init__(self, line):
        parts = line.split()
        self.type, self.procs = parts[1], int(parts[2])
        self.distance, self.iters = int(parts[3]), int(parts[4])
        self.proc_time, self.msg_time, self.total_time = int(parts[5]), int(parts[6]), int(parts[7])       
        
# Initiate the Font properties for the plots
def initiate_plot_fonts():
    font = {'family' : 'Consolas', 'size' : 8}
    matplotlib.rc('font', **font)
    
def filter(value, type):
    # Was used initially to clear unwanted values from the code dump
    return value

# Parse the code dump and create a manageable structure
def parse_data_files(data_file_prefix):
    timers = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
    for file in glob.glob(data_file_prefix):
        with open(file) as F:
            print file
            for line in F:
                if line.startswith(OUT_DELIMITER):
                    line_info = TimingInfo(line)
                    timers[line_info.type][line_info.procs][TimingInfo.PROC_TIME].append(filter(line_info.proc_time, line_info.type))
                    timers[line_info.type][line_info.procs][TimingInfo.MSG_TIME].append(filter(line_info.msg_time, line_info.type))
                    timers[line_info.type][line_info.procs][TimingInfo.TOTAL_TIME].append(filter(line_info.total_time, line_info.type))
                    timers[line_info.type][line_info.procs][TimingInfo.DISTANCE].append(filter(line_info.distance, line_info.type))
    
    return timers

# Plot the Time and Speedup  Comparison for an algorithm along with base type   
def plot_results(base_type, test_type, file_name, figure_title):
    timers = parse_data_files('batch_job_results/*')
    num_procs = sorted(set([1, 2, 4, 8, 16, 32, 64, 128]).
                       intersection(set(timers[test_type].keys())))
    
    base_time = float(mean(timers[base_type][1][TimingInfo.TOTAL_TIME]))
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    
    ax1.plot(num_procs, [mean(timers[test_type][proc][TimingInfo.TOTAL_TIME]) 
                for proc in num_procs], label='Time for %s' % (test_type), marker='o', color='r')
    
    ax1.axhline(base_time, 
                linestyle='--', color='r', label='Time for %s' % (base_type))

    
    ax2.plot(num_procs, [base_time/mean(timers[test_type][proc][TimingInfo.TOTAL_TIME]) for proc in num_procs], 
             label='Speedup for %s' % (test_type), marker='x', color='g')
    
    ax2.set_ylim([0, 128])
    ax1.set_xlim([0, 128])
    ax2.plot(ax2.get_xlim(), ax2.get_ylim(), ls="--", color='g', c=".3", label='Linear Speedup')

        
    ax1.set_xlabel('# Processors', fontsize=12)
    ax1.set_ylabel('Time (in usecs)', fontsize=12)
    ax2.set_ylabel('Speedups', fontsize=12)
    
    ax1.legend(loc='lower center', bbox_to_anchor=(0.2, 1), ncol=2, 
              fancybox=True, shadow=True, title='%s Times' % (figure_title))
    
    ax2.legend(loc='lower center', bbox_to_anchor=(0.8, 1), ncol=2, 
              fancybox=True, shadow=True, title='%s Speedups' % (figure_title))
        
    plt.savefig('figures/%s.png'%(file_name))
    plt.show()
    plt.clf()
    
# Plot the Timing Results between MPI & OpenMP   
def plot_time_comps():
    timers = parse_data_files('batch_job_results/*')
    num_procs = set([1, 2, 4, 8, 16, 32, 64, 128])
    for type in [TimingInfo.OMP, TimingInfo.MPI]:
        num_procs = num_procs.intersection(set(timers[type].keys()))
        
    num_procs = sorted(num_procs)
    
    for type in [TimingInfo.OMP, TimingInfo.MPI]:
        plt.plot(num_procs, [mean(timers[type][proc][TimingInfo.PROC_TIME]) for proc in num_procs], 
                 label='%s - Processing' % (type), marker='o')
        plt.plot(num_procs, [mean(timers[type][proc][TimingInfo.MSG_TIME]) for proc in num_procs], 
                 label='%s - Communication' % (type), marker='+')
        plt.plot(num_procs, [mean(timers[type][proc][TimingInfo.TOTAL_TIME]) for proc in num_procs], 
                 label='%s - Total' % (type), marker='x')
    
    plt.title('Time Comparisons')

    ax = plt.subplot(111)
    ax.set_ylabel('Time Taken (usecs)', fontsize=12)
    ax.set_xlabel('# Processors', fontsize=12)
    
    ax.legend(loc='lower center', bbox_to_anchor=(0.5, 1), ncol=3, 
              fancybox=True, shadow=True)

    plt.savefig('figures/mpi-omp-comp.png')
    plt.show()
    plt.clf()
    
if __name__ == '__main__':
    initiate_plot_fonts()
    plot_time_comps()
    plot_results(TimingInfo.SEQ, TimingInfo.OMP, 'bidirectional-perf', 'Bidirectional Search')    
    plot_results(TimingInfo.SEQ_A, TimingInfo.OMP_A, 'bidirectional-astar-perf', 'Bidirectional + A* Search')