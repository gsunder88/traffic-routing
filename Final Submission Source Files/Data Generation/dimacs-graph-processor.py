'''
Created on Apr 23, 2015

@author: ga-capar
'''

from os import path
from utils import *
import glob

INPUT_FILE_PREFIX = path.join('sample-data', 'heavy-data', 'us-east-splits', '*.gr_*')
LAT_LON_FILE = path.join('sample-data', 'heavy-data', 'us-east-splits', 'USA-road-d.E.co')

num_procs = 128
PARTITION_FILE = path.join('sample-data', 'heavy-data', 'us-east-data', 'adjacency_list_us_east.%s.%d'%('part', num_procs))

# Read the line by line partition info dumped by gpmetis
def read_partition_info():
    partition_info = {}
    
    idx = 1
    with open(PARTITION_FILE) as F:
        for line in F:
            partition_info[idx] = int(line)
            idx += 1
        
    return partition_info

# Order the node ids so that node_id%num_procs gives the owning processor_id
def renumber_nodes_by_partition():
    partitioned_nodes = defaultdict(list)
    
    idx = 1
    with open(PARTITION_FILE) as F:
        for line in F:
            partitioned_nodes[int(line)].append(idx)
            idx += 1
            
        
    inverted_node_ids = {}
    for partition in partitioned_nodes:
        idx = partition + 1
        for node in partitioned_nodes[partition]:
            inverted_node_ids[node] = idx
            idx += num_procs
            
    return inverted_node_ids
            

# Read in the adjacency list file
def parse_data():
    adjacency_list = defaultdict(list)

    line_num = 0
    for fl in glob.glob(INPUT_FILE_PREFIX): 
        print 'Processing file %s' % (fl)
        
        with open(fl) as F:
            for line in F:
                line_num += 1
                parts = line.split()
                adjacency_list[int(parts[1])].append((int(parts[2]), int(parts[3])))
                
                if line_num % 100000 == 0:
                    print 'Processed %d lines' % line_num

    print 'Graph contains %d nodes and %d edges' % (len(adjacency_list), line_num)
    return (adjacency_list, line_num)

# Reorder the adjacency list based on the partitioned node ids 
def invert_adjacency_list(adjacency_list, inverted_node_ids):
    new_adjacency_list = defaultdict(list)
    for node in adjacency_list:
        new_node_id = inverted_node_ids[node]
        for (nei, weight) in adjacency_list[node]:
            new_adjacency_list[new_node_id].append((inverted_node_ids[nei], weight))
    
    return new_adjacency_list

# Dump the processed adjacency list to files
def dump_adjacency_list(adjacency_list, edges, output_file, print_reverse=True):
    edge_cuts = 0
    max_degree, count = 0, 0
    
    with open(output_file, 'w') as F:
        F.write('%d %d\n' % (max(adjacency_list.keys()), edges/2))
        for node in xrange(1, max(adjacency_list.keys())+1):
            neighbour_info = []
            max_degree = max(max_degree, len(adjacency_list[node]))
            for (nei, weight) in adjacency_list[node]:
                if print_reverse or nei > node:
                    neighbour_info.append('%d %d' % (nei, weight))
                    
                if nei % num_procs != node % num_procs:
                    edge_cuts += 1;
            
            F.write(' '.join(neighbour_info) + '\n')    
            count += len(neighbour_info)
    
    print 'Highest degree for any node is %d out of %d edges with %d edge-cuts' % (max_degree, count, edge_cuts)
    
# Instrument the number of edge cuts before and after partitioning
def count_edge_cuts(adjacency_list, partition_info):
    edge_cuts = 0
    for node in adjacency_list:
        for (nei, weight) in adjacency_list[node]:
            edge_cuts += (partition_info[node] != partition_info[nei])
            
    print 'Found total of %d edge-cuts' % (edge_cuts)
    
# Reorder the lat/lon file after partitioning
def restructure_lat_lon_file(inverted_node_ids):
    lat_lon_info = {}
    with open(LAT_LON_FILE) as INP:
        for line in INP:
            parts = line.split()
            lat_lon_info[inverted_node_ids[int(parts[1])]] = (int(parts[2]), int(parts[3]))
    
    missed = 0
    with open(LAT_LON_FILE+'.part', 'w') as OUT:
         for node in xrange(1, max(inverted_node_ids.values())+1):
             if node not in lat_lon_info:
                 missed += 1
                 OUT.write('\n')
             else:
                 OUT.write('%d %d\n' % (lat_lon_info[node][0], lat_lon_info[node][1]))
                 
    print '%d nodes missed!' % (missed)
    
if __name__ == '__main__':
    IN_REPARTITION_MODE = True
    (adjacency_list, edges) = parse_data()
    
    if IN_REPARTITION_MODE: 
        print 'Inverting the Node Ids'
        
        # Renumber and align the adjacency list for edge-cut minimisation
        inverted_node_ids = renumber_nodes_by_partition()
        partition_info = read_partition_info()
             
        count_edge_cuts(adjacency_list, partition_info)
        adjacency_list = invert_adjacency_list(adjacency_list, inverted_node_ids)
             
        print '%d nodes are missing' % (max(adjacency_list.keys()) - len(adjacency_list))
     
    ADJACENCY_FILE = 'adjacency_list_us_east' + ('.edges' if IN_REPARTITION_MODE else '')
    dump_adjacency_list(adjacency_list, edges, ADJACENCY_FILE, print_reverse=True)
      
    print 'Labelling the graph'
    node_labels = label_nodes(adjacency_list, True)
    
    if IN_REPARTITION_MODE:
        print 'Inverting Lat/Lon Info'
        
        # Clear off unwanted memory
        adjacency_list, node_labels, partition_info = None, None, None
        restructure_lat_lon_file(inverted_node_ids)