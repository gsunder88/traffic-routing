'''
Created on Apr 20, 2015
@author: ga-capar
'''

from os import path
import xml.etree.ElementTree as XML
from utils import *

INPUT_FILE_XML = path.join('sample-data', 'output.osm')

# Parse XML output from OSMOSIS
def parse_data():
    adjacency_list = defaultdict(list)

    tree = XML.parse(INPUT_FILE_XML)
    root = tree.getroot()
    
    for child in root:
        if child.tag == 'way':
            way_nodes = list(child.iter('nd'))
            st, end = way_nodes[0].attrib['ref'], way_nodes[len(way_nodes)-1].attrib['ref']
            adjacency_list[st].append(end)
            adjacency_list[end].append(st)
    
    return adjacency_list
        
# Parse the input file - make sure XML segments are removed from memory section-wise to make it more efficient    
def parse_data_online():
    adjacency_list = defaultdict(list)

    node_count = 0
    for event, elem in XML.iterparse(INPUT_FILE_XML, events=("start",)):
        node_count += 1
        if elem.tag == 'way':
            way_nodes = list(elem.iter('nd'))
            if len(way_nodes) == 0:
                continue
            
            st, end = way_nodes[0].attrib['ref'], way_nodes[-1].attrib['ref']
            adjacency_list[st].append(end)
            adjacency_list[end].append(st)
        
        elem.clear()
        if node_count % 1000 == 0:
            print 'Processed %d nodes' % (node_count)
            
    return adjacency_list

if __name__ == '__main__':
    adjacency_list = parse_data_online()
    node_labels = label_nodes(adjacency_list)
    
    print len(adjacency_list), len(node_labels)        
