#include "min_heap.h"
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>

using namespace std;

unordered_map<int, vector< pair<int, int> > > adjacency_list;
int vertices, edges;

string input_files[2] = {"/home/ga-capar/workspace/MinHeap/src/data-files/graph1",
						 "/home/ga-capar/workspace/MinHeap/src/data-files/adjacency_list_newyork"};

ofstream debugger("debug.out");

void read_adjacency_list() {
	ifstream graph(input_files[1]);

	string line;

	getline(graph, line);
	stringstream meta(line);
	meta >> vertices >> edges;

	int node_id = 1;

//	debugger << "Adjacency List:" << endl;
	for (int vert = 0; vert < vertices; vert += 1) {
		getline(graph, line);
		stringstream vinfo(line);
		int nei, weight;

		while (vinfo >> nei) {
			vinfo >> weight;

			// Assuming an undirected graph here!
			adjacency_list[node_id].push_back(make_pair(nei, weight));
			adjacency_list[nei].push_back(make_pair(node_id, weight));
		}

//		debugger << node_id << ":";
//		for (auto nei: adjacency_list[node_id])
//			debugger << nei.first << " " << nei.second << " ";
//		debugger << endl;

		node_id += 1;
	}
}

void dijkstra(int src, int dest) {
	hash_table node_dist, prev;
	min_heap heap(node_dist);

	node_dist[src] = 0;
	prev[src] = -1;
	heap.push(src);

	while (heap.get_size()) {
		int node = heap.extract_min();
//		debugger << "Extracted Node: " << node << " of weight: " << node_dist[node] << endl;

		if (node == dest)
			break;

		for (auto nei : adjacency_list[node]) {
			int dist = node_dist[node] + nei.second;
			if (node_dist.find(nei.first) == node_dist.end()
					|| dist < node_dist[nei.first]) {
				node_dist[nei.first] = dist;
				prev[nei.first] = node;
				heap.decrement_key(nei.first);
//				debugger << "\tUpdating Node: " << nei.first << " with weight: "
//						<< node_dist[nei.first] << endl;
			}
		}
	}

	cout << "Distance to Node: " << dest << " is " << node_dist[dest] << endl;
	cout << "Path Info:" << endl;

	vector<int> path;
	while (dest != -1) {
		path.push_back(dest);
		dest = prev[dest];
	}

	reverse(path.begin(), path.end());
	for (auto node : path)
		cout << node << " ";
	cout << endl;
}

int run_djikstra() {
	read_adjacency_list();

	while (true) {
		int src, dest;
		cin >> src >> dest;
		dijkstra(src, dest);
	}

	return 0;
}
