#!/bin/bash
if [ "$#" -ne 3 ]; then
    echo "Usage: ./<file_name>.sh <adjacency_list_file> <split_file_prefix> <num_procs>"
    exit 1
fi

line_num=1
cat $1 | while read LINE
do
  echo "$LINE" >> $2_$3_$(($line_num%$3))
  let line_num=line_num+1
done
