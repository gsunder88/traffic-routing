/*
 *  prefix_openmp.cpp - Computes the PREFIX SUMs using the Hypercube model on OPENMP.
 */

/*---------------------------------------------------------
 *  Parallel Prefix Sums
 *
 *  1. Each thread generates numints/nprocs random integers (in parallel OpenMP region)
 *  2. Each thread sums his numints/nprocs random integers (in parallel OpenMP region)
*   3. Use the Hypercube model to propagate intermediate results along the cube levels.
 *  3  It takes O(numints/nprocs + nprocs + log(nprocs)) time
 *
 *  NOTE: steps 2-3 are repeated as many times as requested (numiterations)
 *---------------------------------------------------------*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <omp.h>
#include <vector>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <functional>

using namespace std;

/*==============================================================
 * print_elapsed (prints timing statistics)
 *==============================================================*/
void print_elapsed(const char* desc, struct timeval* start, struct timeval* end, int niters) {

  struct timeval elapsed;
  /* calculate elapsed time */
  if(start->tv_usec > end->tv_usec) {

    end->tv_usec += 1000000;
    end->tv_sec--;
  }
  elapsed.tv_usec = end->tv_usec - start->tv_usec;
  elapsed.tv_sec  = end->tv_sec  - start->tv_sec;

  printf("\n %s total elapsed time = %ld (usec)\n",
    desc, (elapsed.tv_sec*1000000 + elapsed.tv_usec) / niters);
}

// Functor to sum the numbers
template<typename T>
struct sum_functor {

  // Constructor
  sum_functor() : m_sum(0) {
  }

  void operator() (int& num) {
    m_sum +=num;
  }

  T get_sum() const {
    return m_sum;
  }

  protected:

  T m_sum;
};

/*
 * Main Program (Parallel Prefix Sums)
 */
int main(int argc, char *argv[]) {

  int numints = 0;
  int numiterations = 0;

  vector<int> data;
  vector<long> partial_sums;
  vector<long> prefix_sums; /* Structure will hold the thread-wise prefix sum used for intermediate computations */
  vector<long> psums; /* Structure will hold the final prefix sum value for each position */

  const char *inputf = string("INPUT_OPENMP").c_str(), *outputf = string("OUTPUT_OPENMP").c_str();
  ofstream ihandle, ohandle;
  ihandle.open(inputf, ios::out), ohandle.open(outputf, ios::out);

  struct timeval start, end;   /* gettimeofday stuff */
  struct timezone tzp;

  int num_threads = omp_get_max_threads();

  if( argc < 3) {
    printf("Usage: %s [numints] [numiterations]\n\n", argv[0]);
    exit(1);
  }

  numints       = atoi(argv[1]);
  numiterations = atoi(argv[2]);

  printf("\nExecuting %s: nthreads=%d, numints=%d, numiterations=%d\n",
            argv[0], num_threads, numints, numiterations);

  data.reserve(numints);
  psums.reserve(numints);

  partial_sums.resize(num_threads);
  prefix_sums.resize(num_threads);

  int block = numints/num_threads;
  int last_block = numints - block * (num_threads-1);

  /*****************************************************
   * Generate the random ints in parallel              *
   *****************************************************/
  #pragma omp parallel shared(numints,data,block,last_block)
  {
    int tid;

    /* get the current thread ID in the parallel region */
    tid = omp_get_thread_num();
    cout << "Processor: " << tid << endl;

    srand(tid + time(NULL));    /* Seed rand functions */

    vector<int>::iterator it_cur = data.begin();
    std::advance(it_cur, tid * block);

    vector<int>::iterator it_end = it_cur;
    std::advance(it_end, tid == num_threads-1 ? last_block : block);

    for(; it_cur != it_end ; ++it_cur) {

      *it_cur = rand();
    }
  }

  /* Output the generated random integers */
  for (int idx = 0; idx < numints; idx += 1)
    ihandle << data[idx] << " ";
  ihandle << endl;
  ihandle.close();

  gettimeofday(&start, &tzp);

  for(int iteration=0; iteration < numiterations; ++iteration) {

    /* Compute the block sums for each block of ints owned by a thread */
    #pragma omp parallel shared(numints,data,partial_sums,block,last_block)
    {
      int tid;

      /* get the current thread ID in the parallel region */
      tid = omp_get_thread_num();

      vector<int>::iterator it_cur = data.begin();
      std::advance(it_cur, tid * block);

      vector<int>::iterator it_end = it_cur;
      std::advance(it_end, tid == num_threads-1 ? last_block : block);

      sum_functor<long> result = std::for_each(it_cur, it_end, sum_functor<long>());

      /* Write the partial result to share memory */
      partial_sums[tid] = result.get_sum();

      /* Initialise all the prefix_sum elements to 0 for the next computation */
      prefix_sums[tid] = 0;
    }

    /* Hypercube based Prefix calculations among all the procs */
    #pragma omp parallel shared(numints, partial_sums, prefix_sums)
    {
      int tid = omp_get_thread_num();

      for (int bit = 0; (1 << bit) < num_threads; bit += 1) {
        int pair = tid ^ (1 << bit);

        long to_add = pair < num_threads ? partial_sums[pair] : 0;

        /* Ensure every thread syncs up at each of the log(nprocs) levels after the read */
        #pragma omp barrier

        partial_sums[tid] += to_add;
        if (pair < tid)
          prefix_sums[tid] += to_add;

        /* Ensure thread sync up for each level */
        #pragma omp barrier
     }

      /* Update the psums with the actual prefix for each position using the block prefix_sums
       * and the running sums within a block. This is also parallel.
       */
      long running = 0;
      for (int idx = tid * block; idx < tid*block + (tid == num_threads-1 ? last_block : block); idx += 1) {
        psums[idx] = data[idx] + prefix_sums[tid] + running;
        running += data[idx];
      }
    }

    /* Validator in Main thread to ensure the results match expectations. */
    long running = 0;
    for (int idx = 0; idx < numints; idx += 1) {
      running += data[idx];
      if (psums[idx] != running) {
        cout << "Failure with mismatch in value at index: " << idx << " need: " << running << " saw: " << psums[idx] << endl;
        return 0;
      }
    }

  }

  gettimeofday(&end,&tzp);

  /*****************************************************
   * Output timing results                             *
   *****************************************************/

  print_elapsed("Prefix Summation", &start, &end, numiterations);

  /* Print the prefix sums for the generated integers */
  for (int idx = 0; idx < numints; idx += 1)
    ohandle << psums[idx] << " ";
  ohandle << endl;
  ohandle.close();

  return(0);
}
