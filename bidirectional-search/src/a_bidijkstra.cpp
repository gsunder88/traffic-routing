#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <omp.h>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <numeric>
#include <functional>
#include <climits>
#include <utility>
#include <sstream>

#include "min_heap.h"
#include <unordered_map>
#include <vector>
#include "utils.h"

using namespace std;

enum direction {
	FORWARD, BACKWARD, NUM_DIRECTIONS
};

string ADJ_FILE_PREFIX, LOC_FILE_PREFIX;
const int NUM_ITERS = 10;

#define PROC_HEADER string("Processor: " + to_string(my_proc_id) + " ")

// Find the min. distance from all the processors
int get_min_distance(int *min_distances, int NUM_PROCS) {
	int min_proc = -1, minimum_distance = INT_MAX;
	for (int proc_id = 0; proc_id < NUM_PROCS; proc_id += 1) {
		if (min_distances[proc_id] < minimum_distance)
			minimum_distance = min_distances[proc_id], min_proc = proc_id;
	}

	return minimum_distance;
}

// Include A* heuristics into the Parallel Bidirectional Search
vector <int> BiDirectionalDijkstra(vector <int> from, vector <int> to) {
	vector <int> results;
	int NUM_PROCS = omp_get_max_threads();

	vector<pair<int, int> > cross_partition_nodes[NUM_PROCS][NUM_PROCS][NUM_DIRECTIONS];
	int sources[NUM_DIRECTIONS];

	int min_distances[NUM_PROCS], queue_sizes[NUM_PROCS];
	int total_q_size, minimum_distance;
	int iters_without_improvement;
	pair <int, int> source_locs[NUM_DIRECTIONS];

	#pragma omp parallel shared(cross_partition_nodes, min_distances, total_q_size, minimum_distance, source_locs, queue_sizes, sources, NUM_PROCS, from, to, results, iters_without_improvement)
	{
		int my_proc_id = omp_get_thread_num();

		UMAP<int, UMAP<int, int> > adjacency_list;
		UMAP<int, pair<int, int> > locations;

		load_adjacency_list(adjacency_list, ADJ_FILE_PREFIX, my_proc_id, NUM_PROCS);
		load_latlong(locations, LOC_FILE_PREFIX, my_proc_id, NUM_PROCS);

		struct timeval start, end;   /* gettimeofday stuff */
		struct timezone tzp;
		long elapsed_usecs_comp, elapsed_usecs_msg;

		for (int input_id = 0; input_id < from.size(); input_id += 1) {
			elapsed_usecs_comp = 0, elapsed_usecs_msg = 0;
			int iter;

			for (int repeat = 0; repeat < NUM_ITERS; repeat += 1) {
				if (my_proc_id == 0) {
					total_q_size = 2, minimum_distance = INT_MAX;
					iters_without_improvement = 0;

					fill_n(min_distances, NUM_PROCS, INT_MAX), fill_n(queue_sizes, NUM_PROCS, 0);

					for (int pid1 = 0; pid1 < NUM_PROCS; pid1 += 1)
						for (int pid2 = 0; pid2 < NUM_PROCS; pid2 += 1)
							for (int dir = 0; dir < NUM_DIRECTIONS; dir += 1)
								cross_partition_nodes[pid1][pid2][dir].clear();

					sources[FORWARD] = from[input_id], sources[BACKWARD] = to[input_id];
				}

				/* Sync at the beginning of the iteration */
				#pragma omp barrier

				UMAP<int, int> node_weights[NUM_DIRECTIONS];
				min_heap pqueues[NUM_DIRECTIONS] = { min_heap(node_weights[FORWARD]),
						min_heap(node_weights[BACKWARD]) };

				iter = 0;
				for (int dir = 0; dir < NUM_DIRECTIONS; dir += 1) {
					if (GET_PARTITION(sources[dir], NUM_PROCS) == my_proc_id) {
						source_locs[dir] = locations[sources[dir]];
					}
				}

				/* Ensure thread sync up after reading graph */
				#pragma omp barrier

				for (int dir = 0; dir < NUM_DIRECTIONS; dir += 1) {
					if (GET_PARTITION(sources[dir], NUM_PROCS) == my_proc_id) {
						node_weights[dir][sources[dir]] = get_distance(source_locs[dir], source_locs[!dir]);
						pqueues[dir].push(sources[dir]);
					}
				}

				while (total_q_size > 0 && (minimum_distance == INT_MAX)) { // || iters_without_improvement <= THRESHOLD)) {
					iter += 1;

					if (my_proc_id == 0)	gettimeofday(&start, &tzp);

					for(int current_direction = 0; current_direction < NUM_DIRECTIONS; current_direction += 1) {
						if (pqueues[current_direction].get_size() == 0)	continue;

						int node = pqueues[current_direction].extract_min();

						// When outside the queue, its weight needs to be the real weight
						node_weights[current_direction][node] -= get_distance(locations[node], source_locs[!current_direction]);

						if (!pqueues[!current_direction].in_queue(node) &&
								node_weights[!current_direction].find(node) != node_weights[!current_direction].end()) {
							// Node is already processed in other direction
							min_distances[my_proc_id] =
									min(min_distances[my_proc_id],
										node_weights[current_direction][node] + node_weights[!current_direction][node]);
						}

						else {
							for (auto elt : adjacency_list[node]) {
								int nei = elt.first, weight = elt.second;

								if (GET_PARTITION(nei, NUM_PROCS) != my_proc_id) {
									// If the neighbour belongs to another processor, send the original weight only
									cross_partition_nodes[my_proc_id][GET_PARTITION(nei, NUM_PROCS)][current_direction].push_back(
											make_pair(nei, weight + node_weights[current_direction][node]));
								}
								else {
									if (node_weights[current_direction].find(nei)
											== node_weights[current_direction].end()
											|| node_weights[current_direction][nei]
													> node_weights[current_direction][node]
															+ weight + get_distance(locations[nei], source_locs[!current_direction])) {
										node_weights[current_direction][nei] =
												node_weights[current_direction][node]
														+ weight + get_distance(locations[nei], source_locs[!current_direction]);
										pqueues[current_direction].decrement_key(nei);
									}
								}
							}
						}
					}

					/* Sync before reading cross-partition-nodes */
					#pragma omp barrier

					if (my_proc_id == 0) {
						gettimeofday(&end, &tzp);
						elapsed_usecs_comp += get_elapsed(&start, &end);
						gettimeofday(&start, &tzp);
					}

					for (int dir = 0; dir < NUM_DIRECTIONS; dir += 1) {
						for (int other_pid = 0; other_pid < NUM_PROCS; other_pid += 1) {
							if (other_pid == my_proc_id)
								continue;

							for (int idx = 0; idx < cross_partition_nodes[other_pid][my_proc_id][dir].size(); idx++) {
								int node = cross_partition_nodes[other_pid][my_proc_id][dir][idx].first,
									weight = cross_partition_nodes[other_pid][my_proc_id][dir][idx].second;

								if (node_weights[dir].find(node) == node_weights[dir].end()
										|| node_weights[dir][node] > weight + get_distance(locations[node], source_locs[!dir])) {

									node_weights[dir][node] = weight + get_distance(locations[node], source_locs[!dir]);
									pqueues[dir].decrement_key(node);
								}
							}

							cross_partition_nodes[other_pid][my_proc_id][dir].clear();
						}
					}

					queue_sizes[my_proc_id] = pqueues[FORWARD].get_size() + pqueues[BACKWARD].get_size();

					/* Ensure everyone is complete processing their in-bound messages */
					#pragma omp barrier

					if (my_proc_id == 0) {
						gettimeofday(&end, &tzp);
						elapsed_usecs_msg += get_elapsed(&start, &end);

						total_q_size = accumulate(queue_sizes, queue_sizes + NUM_PROCS, 0);
						minimum_distance = get_min_distance(min_distances, NUM_PROCS);
					}

					/* Sync up at the end of the iteration */
					#pragma omp barrier
				}

				if (my_proc_id == 0) {
					results.push_back(minimum_distance);
				}
			}

			if (my_proc_id == 0) {
				elapsed_usecs_comp /= NUM_ITERS, elapsed_usecs_msg /= NUM_ITERS;
				cout << OUT_DEL << "OMP_A " << NUM_PROCS << " " << abs(minimum_distance) << " " << iter << " "
						<< elapsed_usecs_comp << " " << elapsed_usecs_msg << " " << (elapsed_usecs_comp + elapsed_usecs_msg) << endl;
			}

		}

	} // pragma shared block ends

	return results;
}

int main(int argc, char *argv[]) {
	cin >> ADJ_FILE_PREFIX;
	LOC_FILE_PREFIX = ADJ_FILE_PREFIX + "_co";

	cout << "#procs: " << omp_get_max_threads() << endl;

	int T;
	cin >> T;
	vector <int> from(T), to(T);
	for (int i = 0; i < T; i += 1)
		cin >> from[i] >> to[i];

	BiDirectionalDijkstra(from, to);
}
