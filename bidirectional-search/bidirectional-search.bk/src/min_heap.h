//
// Created by ga-capar on 4/24/15.
//

#ifndef MINHEAP_MIN_HEAP_H
#define MINHEAP_MIN_HEAP_H

#define LEFT(i) (2*i + 1)
#define RIGHT(i) (LEFT(i) + 1)
#define PARENT(i) (i-1)/2

#endif //MINHEAP_MIN_HEAP_H

#include <unordered_map>
#include <vector>
#include <iostream>

using namespace std;

typedef unordered_map<int, int> hash_table;

class min_heap {
    hash_table &weight_table;
    hash_table position_table;
    vector<int> heap_store;
    int __size;
    int __block_size;

    void heapify_up(int index) {
        while (index > 0) {
            if (weight_table[heap_store[index]] < weight_table[heap_store[PARENT(index)]]) {
                swap(position_table[heap_store[index]], position_table[heap_store[PARENT(index)]]);
                swap(heap_store[index], heap_store[PARENT(index)]);
                index = PARENT(index);
            }

            else
                break;
        }
    }

    void heapify_down(int index) {
        while (LEFT(index) < __size) {
            int min_index = index;
            if (weight_table[heap_store[min_index]] > weight_table[heap_store[LEFT(index)]])
                min_index = LEFT(index);

            if (RIGHT(index) < __size &&
                weight_table[heap_store[min_index]] > weight_table[heap_store[RIGHT(index)]])
                min_index = RIGHT(index);

            if (index == min_index)
                break;

            swap(position_table[heap_store[index]], position_table[heap_store[min_index]]);
            swap(heap_store[index], heap_store[min_index]);
            index = min_index;
        }
    }

public:
    min_heap(hash_table &__weight_lookup, int block_size = 1000)
            : weight_table(__weight_lookup), __block_size(block_size) {
        heap_store.reserve(__block_size);
        __size = 0;
    }

    int get_size() {
        return __size;
    }

    int peek_min() {
        if (__size == 0)
            throw "Heap Empty!";

        return heap_store[0];
    }

    int extract_min() {
        if (__size == 0)
            throw "Heap Empty!";

        int ret = heap_store[0];
        position_table.erase(ret);

        if (__size > 1) {
			heap_store[0] = heap_store[__size - 1];
			position_table[heap_store[0]] = 0;
        }

        __size -= 1;
        heapify_down(0);

        return ret;
    }

    void push(int key) {
        if (__size == heap_store.capacity())
            heap_store.reserve(__size + __block_size);

        heap_store[__size] = key;
        position_table[key] = __size;

        __size += 1;
        heapify_up(__size - 1);
    }

    void decrement_key(int key) {
        if (position_table.find(key) == position_table.end()) {
            push(key);
            return;
        }

        heapify_up(position_table[key]);
    }

    bool in_queue(int key) {
    	return position_table.find(key) != position_table.end();
    }

    void dump() {
        cout << "HEAP SIZE: " << __size << endl;

        for (int i = 0; i < __size; i += 1)
            cout << heap_store[i] << "(" << weight_table[heap_store[i]] << ") ";
        cout << endl;
    }

//    string get_dump() {
//    	if (__size == 0)	return " Empty";
//    	string contents;
//    	for (int i = 0; i < __size; i += 1)
//    		contents += to_string(heap_store[i]) + ":" + to_string(weight_table[heap_store[i]]) + ",";
//    	return contents;
//    }
};

void run_min_heap_tests() {
    hash_table kv_table;
    min_heap heap(kv_table);

    while (true) {
        int command, key, value;
        cin >> command;
        switch (command) {
            case 1:
                // PUSH
                cin >> key;
                kv_table[key] = key;
                heap.push(key);
                break;

            case 2:
                // PEEK MIN
                cout << heap.peek_min() << endl;
                break;

            case 3:
                // EXTRACT MIN
                cout << heap.extract_min() << endl;
                heap.dump();
                break;

            case 4:
                // UPDATE KEY
                cin >> key >> value;
                kv_table[key] = value;
                heap.decrement_key(key);
                break;

            case 5:
                // SIZE
                cout << heap.get_size() << endl;
                break;

            case 6:
                // DUMP HEAP CONTENTS
                heap.dump();
                break;

            case 7:
                goto END;
        }
    }

    END:
        cout << "Completed Tests!" << endl;
}
