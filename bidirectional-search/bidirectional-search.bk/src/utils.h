/*
 * utils.h
 *
 *  Created on: Apr 28, 2015
 *      Author: ga-capar
 */

#include <cmath>
#include <fstream>
#include <unordered_map>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <cassert>

#ifndef UTILS_H_
#define UTILS_H_

#define DEG_TO_RAD(ANGLE) ((ANGLE) * M_PI / 180.0)
#define RAD_TO_DEG(ANGLE) ((ANGLE) * 180.0 / M_PI)
#define NORMALIZE_LATLON(val) (double(val)/1e6)

#define UMAP unordered_map
#define OUT_DEL "METRICS "

#endif /* UTILS_H_ */

using namespace std;

#define GET_PARTITION(node, NUM_PROCS) (((node) & (NUM_PROCS-1))) // Assuming NUM_PROCS is a power of 2

/*==============================================================
 * print_elapsed (prints timing statistics)
 *==============================================================*/

int get_elapsed(struct timeval *start, struct timeval *end) {
	struct timeval elapsed;

	/* calculate elapsed time */
	if (start->tv_usec > end->tv_usec) {
		end->tv_usec += 1000000;
		end->tv_sec--;
	}

	elapsed.tv_usec = end->tv_usec - start->tv_usec;
	elapsed.tv_sec = end->tv_sec - start->tv_sec;

	return elapsed.tv_sec * 1000000 + elapsed.tv_usec;
}

void print_elapsed(const char* desc, struct timeval* start, struct timeval* end,
		int niters) {
	printf("\n %s total elapsed time = %d (usec)\n", desc,
			(get_elapsed(start, end)) / niters);
}

/* Haversize formula for distance:
 * http://www.movable-type.co.uk/scripts/latlong.html
 */

int get_distance(pair <int, int> t1, pair <int, int> t2) {
	const double EARTH_RADIUS = 63710000; // Earths Radius in decimeters

	double lat1 = NORMALIZE_LATLON(t1.first), lat2 = NORMALIZE_LATLON(t2.first);
	double lon1 = NORMALIZE_LATLON(t1.second), lon2 = NORMALIZE_LATLON(t2.second);

	double lat_delta = DEG_TO_RAD(lat2 - lat1);
	double lon_delta = DEG_TO_RAD(lon2 - lon1);

	double lat1_radians = DEG_TO_RAD(lat1), lat2_radians = DEG_TO_RAD(lat2);

	double A = sin(lat_delta / 2) * sin(lat_delta / 2)
			+ cos(lat1_radians) * cos(lat2_radians) * sin(lon_delta / 2)
					* sin(lon_delta / 2);
	double C = fabs(2 * atan2(sqrt(A), sqrt(1 - A)));
	return EARTH_RADIUS * C;

//	while (1) {
//		double lat1, lat2, lon1, lon2;
//		cin >> lat1 >> lon1 >> lat2 >> lon2;
//		cout << get_distance(lat1, lon1, lat2, lon2);
//	}

}

void load_latlong(UMAP<int, pair<int, int> > &locs, string file_prefix,
								int proc_id, int num_procs) {
	ifstream infile(file_prefix + "_" + to_string(num_procs) + "_" + to_string(proc_id));
	string line;

	int node_id = proc_id ? proc_id : num_procs;
	int edges = 0, edge_cuts = 0;
	while (getline(infile, line)) {
		stringstream line_info(line);
		int lon, lat;
		line_info >> lon >> lat;

		locs[node_id] = make_pair(lat, lon);
		node_id += num_procs;
	}
}

void load_adjacency_list(UMAP<int, UMAP<int, int> > &adjacency_list,
								string file_prefix, int proc_id, int num_procs) {

	ifstream infile(file_prefix + "_" + to_string(num_procs) + "_" + to_string(proc_id));
	string line;

	int node_id = proc_id ? proc_id : num_procs;
	int edges = 0, edge_cuts = 0;
	while (getline(infile, line)) {
		stringstream line_info(line);
		int to, weight;
		while (line_info >> to) {
			line_info >> weight;
			// Assumes directed edges to be replaced with undirected pairs
			adjacency_list[node_id][to] = weight;
			adjacency_list[to][node_id] = weight;
			edges += 1, edge_cuts += GET_PARTITION(to, num_procs) != GET_PARTITION(node_id, num_procs);
		}

		node_id += num_procs;
	}
}

