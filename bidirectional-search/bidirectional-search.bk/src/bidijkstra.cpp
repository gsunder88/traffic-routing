/*
 * BidirectionalDijkstra.cpp
 *
 *  Created on: Apr 28, 2015
 *      Author: ga-capar
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <omp.h>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <numeric>
#include <functional>
#include <climits>
#include <utility>
#include <sstream>

#include "min_heap.h"
#include <unordered_map>
#include <vector>
#include "utils.h"

using namespace std;

enum direction {
	FORWARD, BACKWARD, NUM_DIRECTIONS
};

/*
 * TODO:
 * 	Cleanup the couts after copying
 * 	Start timing
 * 	Batch input setup to time better
 * 	Finally, remove parallelisation's block!
 */

string ADJ_FILE_PREFIX;
const int NUM_ITERS = 100;

#define PROC_HEADER string("Processor: " + to_string(my_proc_id) + " ")

int get_min_distance(int *min_distances, int NUM_PROCS) {
	int min_proc = -1, minimum_distance = INT_MAX;
	for (int proc_id = 0; proc_id < NUM_PROCS; proc_id += 1) {
		if (min_distances[proc_id] < minimum_distance)
			minimum_distance = min_distances[proc_id], min_proc = proc_id;
	}

	return minimum_distance;
}

vector <int> BiDirectionalDijkstra(vector <int> from, vector <int> to) {
	vector <int> results;
	int NUM_PROCS = omp_get_max_threads();

	vector<pair<int, int> > cross_partition_nodes[NUM_PROCS][NUM_PROCS][NUM_DIRECTIONS];
	int sources[NUM_DIRECTIONS];

	int min_distances[NUM_PROCS], queue_sizes[NUM_PROCS];
	int total_q_size, minimum_distance;
	int iters_without_improvement;

	#pragma omp parallel shared(cross_partition_nodes, min_distances, total_q_size, minimum_distance, queue_sizes, sources, NUM_PROCS, from, to, results, iters_without_improvement)
	{
		int my_proc_id = omp_get_thread_num();
//		cout << "Processor: " << my_proc_id << endl;

		UMAP<int, UMAP<int, int> > adjacency_list;
		load_adjacency_list(adjacency_list, ADJ_FILE_PREFIX, my_proc_id, NUM_PROCS);
		const int THRESHOLD = ceil(sqrt(adjacency_list.size()));

		struct timeval start, end;   /* gettimeofday stuff */
		struct timezone tzp;
		long elapsed_usecs_comp, elapsed_usecs_msg;

		for (int input_id = 0; input_id < from.size(); input_id += 1) {
			elapsed_usecs_comp = 0, elapsed_usecs_msg = 0;
			int iter;

			for (int repeat = 0; repeat < NUM_ITERS; repeat += 1) {
				if (my_proc_id == 0) {
	//				cout << "Finding distance from " << from[input_id] << " to " << to[input_id] << endl;
					total_q_size = 2, minimum_distance = INT_MAX;
					iters_without_improvement = 0;

					fill_n(min_distances, NUM_PROCS, INT_MAX), fill_n(queue_sizes, NUM_PROCS, 0);

					for (int pid1 = 0; pid1 < NUM_PROCS; pid1 += 1)
						for (int pid2 = 0; pid2 < NUM_PROCS; pid2 += 1)
							for (int dir = 0; dir < NUM_DIRECTIONS; dir += 1)
								cross_partition_nodes[pid1][pid2][dir].clear();

					sources[0] = from[input_id], sources[1] = to[input_id];
				}

				/* Sync at the beginning of the iteration */
				#pragma omp barrier

				UMAP<int, int> node_weights[NUM_DIRECTIONS];
				min_heap pqueues[NUM_DIRECTIONS] = { min_heap(node_weights[FORWARD]),
						min_heap(node_weights[BACKWARD]) };

				iter = 0;
				for (int working_pid = my_proc_id; working_pid <= my_proc_id; working_pid += 1) {
					if (my_proc_id == working_pid) {
						for (int dir = 0; dir < NUM_DIRECTIONS; dir += 1) {
							if (GET_PARTITION(sources[dir], NUM_PROCS) == my_proc_id) {
								node_weights[dir][sources[dir]] = 0;
								pqueues[dir].push(sources[dir]);
							}
						}
					}
				}

				/* Ensure thread sync up after reading graph */
				#pragma omp barrier

				while (total_q_size > 0 && (minimum_distance == INT_MAX)) { // || iters_without_improvement <= THRESHOLD)) {
					iter += 1;

					if (my_proc_id == 0)	gettimeofday(&start, &tzp);

					for(int current_direction = 0; current_direction < NUM_DIRECTIONS; current_direction += 1) {
						if (pqueues[current_direction].get_size() == 0)	continue;

						int node = pqueues[current_direction].extract_min();
	//					cout << PROC_HEADER << " Node: " << node << " Direction: " << current_direction << " Weight: "
	//							<< node_weights[current_direction][node] << endl;

						if (!pqueues[!current_direction].in_queue(node) &&
								node_weights[!current_direction].find(node) != node_weights[!current_direction].end()) {
							// Node is already processed in other direction
							min_distances[my_proc_id] =
									min(min_distances[my_proc_id],
										node_weights[current_direction][node] + node_weights[!current_direction][node]);

	//						cout << PROC_HEADER << " Node: " << node << " completes the cycle" << endl;
						}

	//						else if (node == sources[!current_direction]) { // If we reached the source in the other direction
	//							cout << PROC_HEADER << " Found source on the other end!" << endl;
	//							min_distances[my_proc_id] = node_weights[current_direction][node];
	//						}

						else {
	//						cout << PROC_HEADER << " Neighbour Count: " << adjacency_list[node].size() << endl;
							for (auto elt : adjacency_list[node]) {
								int nei = elt.first, weight = elt.second;

	//							cout << PROC_HEADER << " Neighbour: " << nei << endl;
								if (GET_PARTITION(nei, NUM_PROCS) != my_proc_id) {
									// If the neighbour belongs to another processor
									cross_partition_nodes[my_proc_id][GET_PARTITION(nei, NUM_PROCS)][current_direction].push_back(
											make_pair(nei, weight + node_weights[current_direction][node]));

	//								cout << PROC_HEADER << nei << " pushed to " << GET_PARTITION(nei, NUM_PROCS) <<
	//										" weight: " << (weight + node_weights[current_direction][node]) << endl;
								}
								else {
									if (node_weights[current_direction].find(nei)
											== node_weights[current_direction].end()
											|| node_weights[current_direction][nei]
													> node_weights[current_direction][node]
															+ weight) {
										node_weights[current_direction][nei] =
												node_weights[current_direction][node]
														+ weight;
										pqueues[current_direction].decrement_key(nei);

	//									cout << PROC_HEADER << nei << " queued, weight: " << (weight + node_weights[current_direction][node]) << endl;
									}
								}
							}
						}
					}

					/* Sync before reading cross-partition-nodes */
					#pragma omp barrier

					if (my_proc_id == 0) {
						gettimeofday(&end, &tzp);
						elapsed_usecs_comp += get_elapsed(&start, &end);
						gettimeofday(&start, &tzp);
					}

					for (int dir = 0; dir < NUM_DIRECTIONS; dir += 1) {
						for (int other_pid = 0; other_pid < NUM_PROCS; other_pid += 1) {
							if (other_pid == my_proc_id)
								continue;

							for (int idx = 0; idx < cross_partition_nodes[other_pid][my_proc_id][dir].size(); idx++) {
								int node = cross_partition_nodes[other_pid][my_proc_id][dir][idx].first,
									weight = cross_partition_nodes[other_pid][my_proc_id][dir][idx].second;

	//							cout << PROC_HEADER << "Direction: " << dir << " Received Node: " << node << " Weight: " << weight << " from Proc: " << other_pid << endl;
								if (node_weights[dir].find(node) == node_weights[dir].end()
										|| node_weights[dir][node] > weight) {

	//								cout << "\t" << PROC_HEADER << "Direction: " << dir << " Pushing Node: " << node << endl;
	//								cout << "\t" << PROC_HEADER << "Direction: " << dir << " Queue before Push: " << pqueues[dir].get_dump() << endl;

									node_weights[dir][node] = weight;
									pqueues[dir].decrement_key(node);

	//								cout << PROC_HEADER << "Direction: " << dir << " Queue after Push: " << pqueues[dir].get_dump() << endl;

								}
								else {
	//								cout << "\t" << PROC_HEADER << "Direction: " << dir << " Skipped Node: " << node << endl;
								}
							}

							cross_partition_nodes[other_pid][my_proc_id][dir].clear();
						}
	//					cout << PROC_HEADER << "Direction: " << dir << " Queue: " << pqueues[dir].get_dump() << endl;
					}

					queue_sizes[my_proc_id] = pqueues[FORWARD].get_size() + pqueues[BACKWARD].get_size();

					/* Ensure everyone is complete processing their in-bound messages */
					#pragma omp barrier

					if (my_proc_id == 0) {
						gettimeofday(&end, &tzp);
						elapsed_usecs_msg += get_elapsed(&start, &end);

	//					cout << "\nIteration: " << iter << endl;
	//
	//					cout << "Min Distances: " << endl;
	//					for (int i = 0; i < NUM_PROCS; i += 1) {
	//						cout << min_distances[i] << " ";
	//					}
	//					cout << endl;
	//
	//					cout << "Queue Sizes: " << endl;
	//					for (int i = 0; i < NUM_PROCS; i += 1) {
	//						cout << queue_sizes[i] << " ";
	//					}
	//					cout << endl;

		//				if (minimum_distance == INT_MAX
		//						&& accumulate(queue_sizes, queue_sizes + NUM_PROCS, 0) == 0)
		//					minimum_distance = INT_MAX - 1; // No possible path - indicate a sentinel as (INT_MAX - 1)

						total_q_size = accumulate(queue_sizes, queue_sizes + NUM_PROCS, 0);
						minimum_distance = get_min_distance(min_distances, NUM_PROCS);

	//					if (new_minimum < minimum_distance) {
	//						iters_without_improvement = 0;
	//						cout << "Improved result at " << iter << " iteration - " << new_minimum << endl;
	//					}
	//					else {
	//						iters_without_improvement += 1;
	//					}
	//
	//					minimum_distance = new_minimum;
	//
	//					if (new_minimum < INT_MAX) {
	//						if (new_minimum == minimum_distance) {
	//							iters_without_improvement += 1;
	////							cout << "Staying back at " << minimum_distance << " for iteration: " << iters_without_improvement << endl;
	//						}
	//						else {
	//							if (minimum_distance < INT_MAX)
	////								cout << "After staying at " << minimum_distance << " for " << iters_without_improvement <<
	////									" iterations, distance reduced to " << new_minimum << endl;
	//							minimum_distance = new_minimum, iters_without_improvement = 0;
	//						}
	//					}

	//					cout << "Iteration: " << iter << " Min Distance: " << get_min_distance(min_distances, NUM_PROCS) << " Queue Size: " << total_q_size << endl;
					}

					/* Sync up at the end of the iteration */
					#pragma omp barrier
				}

				if (my_proc_id == 0) {
					results.push_back(minimum_distance);
				}
			}

			if (my_proc_id == 0) {
				elapsed_usecs_comp /= NUM_ITERS, elapsed_usecs_msg /= NUM_ITERS;
				cout << OUT_DEL << "OMP " << NUM_PROCS << " " << minimum_distance << " " << iter << " "
						<< elapsed_usecs_comp << " " << elapsed_usecs_msg << " " << (elapsed_usecs_comp + elapsed_usecs_msg) << endl;
			}

		}

	} // pragma shared block ends

	return results;
}

int main(int argc, char *argv[]) {
	cin >> ADJ_FILE_PREFIX;
	cout << "#procs: " << omp_get_max_threads() << endl;

	int T;
	cin >> T;
	vector <int> from(T), to(T);
	for (int i = 0; i < T; i += 1)
		cin >> from[i] >> to[i];

	vector <int> results = BiDirectionalDijkstra(from, to);
//	for (int i = 0; i < T; i += 1)
//		cout << "Distance from " << from[i] << " to " << to[i] << " : " << results[i] << endl;
}
