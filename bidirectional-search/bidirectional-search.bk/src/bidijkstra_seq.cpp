/*
 * BidirectionalDijkstra.cpp
 *
 *  Created on: Apr 28, 2015
 *      Author: ga-capar
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <numeric>
#include <functional>
#include <climits>
#include <utility>
#include <sstream>

#include "min_heap.h"
#include <unordered_map>
#include <vector>
#include "utils.h"

using namespace std;

enum direction {
	FORWARD, BACKWARD, NUM_DIRECTIONS
};

int NUM_PROCS;
string ADJ_FILE_PREFIX = "adjacency_file_";

int get_min_distance(int *min_distances) {
	int min_proc = -1, minimum_distance = INT_MAX;
	for (int proc_id = 0; proc_id < NUM_PROCS; proc_id += 1) {
		if (min_distances[proc_id] < minimum_distance)
			minimum_distance = min_distances[proc_id], min_proc = proc_id;
	}

	return minimum_distance;
}

int BiDirectionalDijkstra(int from, int to) {
	vector<pair<int, int> > cross_partition_nodes[NUM_PROCS][NUM_PROCS][NUM_DIRECTIONS];
	int sources[NUM_DIRECTIONS] = { from, to };

	NUM_PROCS = 1;
	int my_proc_id = 0;

	int min_distances[NUM_PROCS], queue_sizes[NUM_PROCS];
	fill_n(min_distances, NUM_PROCS, INT_MAX), fill_n(queue_sizes, NUM_PROCS, 0);

	int minimum_distance = INT_MAX;
	int iters = 0;

	{
		UMAP<int, int> node_weights[NUM_DIRECTIONS];
		min_heap pqueues[NUM_DIRECTIONS] = { min_heap(node_weights[FORWARD]),
				min_heap(node_weights[BACKWARD]) };

		UMAP<int, UMAP<int, int> > adjacency_list;

		load_adjacency_list(adjacency_list, ADJ_FILE_PREFIX, my_proc_id, NUM_PROCS);

		for (int dir = 0; dir < NUM_DIRECTIONS; dir += 1) {
			if (GET_PARTITION(sources[dir], NUM_PROCS) == my_proc_id) {
				node_weights[dir][sources[dir]] = 0;
				pqueues[dir].push(sources[dir]);
			}
		}

		while (minimum_distance == INT_MAX) {
			iters += 1;
			if (pqueues[FORWARD].get_size() + pqueues[BACKWARD].get_size() > 0) {
				int current_direction;
				if (pqueues[FORWARD].get_size() > 0
						&& pqueues[BACKWARD].get_size() > 0) {
					current_direction =
							pqueues[FORWARD].get_size()
									<= pqueues[BACKWARD].get_size() ?
									FORWARD : BACKWARD; // Choose the smaller Queue
				} else {
					current_direction =
							pqueues[FORWARD].get_size() > 0 ?
									FORWARD : BACKWARD; // Choose the non-empty Queue
				}

				int node = pqueues[current_direction].extract_min();
//				cout << "Node: " << node << " processed in direction: " << current_direction << " for iteration: " << iters << endl;

				if (!pqueues[!current_direction].in_queue(node) &&
						node_weights[!current_direction].find(node) != node_weights[!current_direction].end()) {
					// Node is already processed in other direction
					min_distances[my_proc_id] =
							node_weights[current_direction][node] + node_weights[!current_direction][node];
				}

				if (node == sources[!current_direction]) { // If we reached the source in the other direction
					min_distances[my_proc_id] = node_weights[current_direction][node];
				}

				else {
					for (auto elt : adjacency_list[node]) {
						int nei = elt.first, weight = elt.second;

						if (GET_PARTITION(nei, NUM_PROCS) != my_proc_id) {
							// If the neighbour belongs to another processor
							cross_partition_nodes[my_proc_id][GET_PARTITION(nei, NUM_PROCS)][current_direction].push_back(
									make_pair(nei, weight + node_weights[current_direction][node]));
						}
						else {
							if (node_weights[current_direction].find(nei)
									== node_weights[current_direction].end()
									|| node_weights[current_direction][nei]
											> node_weights[current_direction][node]
													+ weight) {
								node_weights[current_direction][nei] =
										node_weights[current_direction][node]
												+ weight;
								pqueues[current_direction].decrement_key(nei);
							}
						}
					}
				}
			}

			queue_sizes[my_proc_id] = pqueues[FORWARD].get_size() + pqueues[BACKWARD].get_size();

			for (int other_pid = 0; other_pid < NUM_PROCS; other_pid += 1) {
				if (other_pid == my_proc_id)
					continue;

				for (int dir = 0; dir < NUM_DIRECTIONS; dir += 1) {
					for (int idx = 0; idx < cross_partition_nodes[other_pid][my_proc_id][dir].size(); idx++) {
						int node = cross_partition_nodes[other_pid][my_proc_id][dir][idx].first,
							weight = cross_partition_nodes[other_pid][my_proc_id][dir][idx].second;

						if (node_weights[dir].find(node) == node_weights[dir].end()
								|| node_weights[dir][node] > weight) {
							node_weights[dir][node] = weight;
							pqueues[dir].decrement_key(node);
						}
					}

					cross_partition_nodes[other_pid][my_proc_id][dir].clear();
				}
			}

			if (my_proc_id == 0) {
				minimum_distance = min(minimum_distance,
						get_min_distance(min_distances));
				if (minimum_distance == INT_MAX
						&& accumulate(queue_sizes, queue_sizes + NUM_PROCS, 0) == 0)
					minimum_distance = INT_MAX - 1; // No possible path - indicate a sentinel as (INT_MAX - 1)
			}
		}

	} // pragma shared block ends

	return minimum_distance;
}

int main() {
	ADJ_FILE_PREFIX = "/home/ga-capar/workspace/Bidirectional_Seq/src/adjacency_list_new_york.edges";
	int T;
	cin >> T;
	while (T -= 1) {
		int from, to;
		cin >> from >> to;
		cout << BiDirectionalDijkstra(from, to) << endl;
	}
}
