/*
 * BidirectionalDijkstra.cpp
 *
 *  Created on: Apr 28, 2015
 *      Author: ga-capar
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <numeric>
#include <functional>
#include <climits>
#include <utility>
#include <sstream>
#include <mpi.h>
#include <cstring>

#include "min_heap.h"
#include <unordered_map>
#include <vector>
#include "utils.h"

using namespace std;

enum direction {
	FORWARD, BACKWARD, NUM_DIRECTIONS
};

/*
 * TODO:
 * 	Cleanup the couts after copying
 * 	Start timing
 * 	Batch input setup to time better
 * 	Finally, remove parallelisation's block!
 */

string ADJ_FILE_PREFIX;
const int NUM_ITERS = 100;

const int MAX_DEGREE = 10;
// Min-Found, Queue-Size, #Nodes, <Node, Weight> * MAX_DEGREE * NUM_DIRECTIONS
enum MESSAGE_INDEX {NUM_NODES = MAX_DEGREE * 2 * NUM_DIRECTIONS, MINIMUM, QUEUE_SIZE, MESSAGE_SIZE};

#define PROC_HEADER string("Processor: " + to_string(my_proc_id) + " ")

int get_min_distance(int *min_distances, int NUM_PROCS) {
	int min_proc = -1, minimum_distance = INT_MAX;
	for (int proc_id = 0; proc_id < NUM_PROCS; proc_id += 1) {
		if (min_distances[proc_id] < minimum_distance)
			minimum_distance = min_distances[proc_id], min_proc = proc_id;
	}

	return minimum_distance;
}

int main(int argc, char **argv) {
	int my_proc_id, NUM_PROCS;

	MPI_Status status;              /* Status variable for MPI operations */

	/*---------------------------------------------------------
	* Initializing the MPI environment
	* "nprocs" copies of this program will be initiated by MPI.
	* All the variables will be private, only the program owner could see its own variables
	* If there must be a inter-procesor communication, the communication must be
	* done explicitly using MPI communication functions.
	*---------------------------------------------------------*/

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_proc_id); /* Getting the ID for this process */

	if (argc != 2) {
		if (my_proc_id == 0) {
			cout << "Usage: ./binary <input_file_name>" << endl;
		}

	    MPI_Finalize();
	    exit(1);
	}

	MPI_Comm_size(MPI_COMM_WORLD, &NUM_PROCS); /* Get number of processors */

	/* Read the input cases from the files */
	string file_name(argv[1]);
	ifstream infile(file_name);

	string ADJ_FILE_PREFIX;
	infile >> ADJ_FILE_PREFIX;

	UMAP<int, UMAP<int, int> > adjacency_list;
	load_adjacency_list(adjacency_list, ADJ_FILE_PREFIX, my_proc_id, NUM_PROCS);

	int T;
	infile >> T;

	vector <int> from(T), to(T);

	for (int idx = 0; idx < T; idx += 1)	infile >> from[idx] >> to[idx];

	int *outbound_message = new int[MESSAGE_SIZE], *inbound_message = new int[MESSAGE_SIZE];
	int sources[NUM_DIRECTIONS];

	MPI_Barrier(MPI_COMM_WORLD); /* Ensure every proc is ready to process */

	struct timeval start, end;   /* gettimeofday stuff */
	struct timezone tzp;
	long elapsed_usecs_comp, elapsed_usecs_msg;

	int minimum_distance, total_qsize;
	for (int input_id = 0; input_id < from.size(); input_id += 1) {
		elapsed_usecs_comp = 0, elapsed_usecs_msg = 0;
		int iter;

		for (int repeat = 0; repeat < NUM_ITERS; repeat += 1) {
//			if (my_proc_id == 0)	cout << "Working on Repetition: " << repeat << endl;

			minimum_distance = INT_MAX, total_qsize = 2;
			sources[FORWARD] = from[input_id], sources[BACKWARD] = to[input_id];

			UMAP<int, int> node_weights[NUM_DIRECTIONS];
			min_heap pqueues[NUM_DIRECTIONS] = { min_heap(node_weights[FORWARD]),
					min_heap(node_weights[BACKWARD]) };

			iter = 0;
			for (int dir = 0; dir < NUM_DIRECTIONS; dir += 1) {
				if (GET_PARTITION(sources[dir], NUM_PROCS) == my_proc_id) {
					node_weights[dir][sources[dir]] = 0;
					pqueues[dir].push(sources[dir]);
				}
			}

			MPI_Barrier(MPI_COMM_WORLD); /* Ensure every proc is initialised */

			while (total_qsize > 0 && minimum_distance == INT_MAX) {
				iter += 1;
//				if (my_proc_id == 0)	cout << "Working on Iteration: " << iter << endl;

				fill_n(outbound_message, MESSAGE_SIZE, 0);

				if (my_proc_id == 0)	gettimeofday(&start, &tzp);

				for(int current_direction = 0; current_direction < NUM_DIRECTIONS; current_direction += 1) {
					if (pqueues[current_direction].get_size() == 0)	continue;

					int node = pqueues[current_direction].extract_min();
//					cout << PROC_HEADER << " Node: " << node << " Direction: " << current_direction << " Weight: "
//							<< node_weights[current_direction][node] << endl;

					if (!pqueues[!current_direction].in_queue(node) &&
							node_weights[!current_direction].find(node) != node_weights[!current_direction].end()) {

						// Node is already processed in other direction
						minimum_distance = min(minimum_distance,
									node_weights[current_direction][node] + node_weights[!current_direction][node]);


//						cout << PROC_HEADER << " Node: " << node << " completes the cycle" << endl;
					}

					else if (node == sources[!current_direction]) { // If we reached the source in the other direction
						minimum_distance = min(minimum_distance, node_weights[current_direction][node]);
					}

					else {
//						cout << PROC_HEADER << " Neighbour Count: " << adjacency_list[node].size() << endl;
						for (auto elt : adjacency_list[node]) {
							int nei = elt.first, weight = elt.second;

//							cout << PROC_HEADER << " Neighbour: " << nei << endl;
							if (GET_PARTITION(nei, NUM_PROCS) != my_proc_id) {
								// If the neighbour belongs to another processor
								outbound_message[outbound_message[NUM_NODES] * 2] = nei * (current_direction == FORWARD ? 1 : -1);
								outbound_message[outbound_message[NUM_NODES] * 2 + 1] = weight + node_weights[current_direction][node];
								outbound_message[NUM_NODES] += 1;

//								cross_partition_nodes[my_proc_id][GET_PARTITION(nei, NUM_PROCS)][current_direction].push_back(
//										make_pair(nei, weight + node_weights[current_direction][node]));
//								cout << PROC_HEADER << nei << " pushed to " << GET_PARTITION(nei, NUM_PROCS) <<
//										" weight: " << (weight + node_weights[current_direction][node]) << endl;
							}
							else {
								if (node_weights[current_direction].find(nei)
										== node_weights[current_direction].end()
										|| node_weights[current_direction][nei]
												> node_weights[current_direction][node]
														+ weight) {
									node_weights[current_direction][nei] =
											node_weights[current_direction][node]
													+ weight;
									pqueues[current_direction].decrement_key(nei);

//									cout << PROC_HEADER << nei << " queued, weight: " << (weight + node_weights[current_direction][node]) << endl;
								}
							}
						}
					}
				}

				MPI_Barrier(MPI_COMM_WORLD); /* Sync up before communication */

				if (my_proc_id == 0) {
					gettimeofday(&end, &tzp);
					elapsed_usecs_comp += get_elapsed(&start, &end);
					gettimeofday(&start, &tzp);
//					cout << "Beginning sync up" << endl;
				}

				total_qsize = 0;
				for (int current_proc_id = 0; current_proc_id < NUM_PROCS; current_proc_id += 1) {

					// Prepare Outbound Message
					if (my_proc_id == current_proc_id) {
						int current_qsize = pqueues[FORWARD].get_size() + pqueues[BACKWARD].get_size();
						total_qsize += current_qsize;
						outbound_message[QUEUE_SIZE] = current_qsize;
						outbound_message[MINIMUM] = minimum_distance;

//						cout << PROC_HEADER << " Sending Message: " << endl;
//						for (int idx = 0; idx < MESSAGE_SIZE; idx += 1)
//							inbound_message[idx] = outbound_message[idx], cout << inbound_message[idx] << " ";
//						cout << endl;

						memcpy(inbound_message, outbound_message, sizeof(int) * MESSAGE_SIZE);

//						cout << PROC_HEADER << " Sending Min Distance: " << minimum_distance
//								<< " QSize: " << current_qsize << " #Nodes: " << inbound_message[NUM_NODES] << endl;
					}

					// If it is my ID, send message out
					MPI_Bcast(inbound_message, MESSAGE_SIZE, MPI_INT, current_proc_id, MPI_COMM_WORLD);

//						for (int other_proc_id = 0; other_proc_id < NUM_PROCS; other_proc_id += 1)
//							if(other_proc_id != my_proc_id)
//								MPI_Send(outbound_message, MESSAGE_SIZE, MPI_INT, other_proc_id, 0, MPI_COMM_WORLD);

					// Process Received Message
					if (my_proc_id != current_proc_id) {
//						cout << PROC_HEADER << " Received message from proc: " << current_proc_id << endl;

//						if (my_proc_id == (current_proc_id + 1) % NUM_PROCS) {
//							cout << PROC_HEADER << " Received Message: " << endl;
//							for (int idx = 0; idx < MESSAGE_SIZE; idx += 1)
//								cout << inbound_message[idx] << " ";
//							cout << endl;
//						}

						total_qsize += inbound_message[QUEUE_SIZE];
						minimum_distance = min(minimum_distance, inbound_message[MINIMUM]);

						for (int idx = 0; idx < inbound_message[NUM_NODES]; idx += 1) {
							int node = inbound_message[idx*2], weight = inbound_message[idx*2 + 1];
							direction dir = FORWARD;

							if (node < 0) dir = BACKWARD, node *= -1;

							if (GET_PARTITION(node, NUM_PROCS) != my_proc_id)	continue;

							if (node_weights[dir].find(node) == node_weights[dir].end()
											|| node_weights[dir][node] > weight) {

//								cout << "\t" << PROC_HEADER << "Direction: " << dir << " Pushing Node: " << node << endl;
//								cout << "\t" << PROC_HEADER << "Direction: " << dir << " Queue before Push: " << pqueues[dir].get_dump() << endl;

								node_weights[dir][node] = weight;
								pqueues[dir].decrement_key(node);

//								cout << PROC_HEADER << "Direction: " << dir << " Queue after Push: " << pqueues[dir].get_dump() << endl;

							}
						}
					}

//					if (my_proc_id == 0) {
//						cout << "Completed the transmission from proc: " << current_proc_id << endl;
//					}

				}

//				for (int dir = 0; dir < NUM_DIRECTIONS; dir += 1) {
//					for (int other_pid = 0; other_pid < NUM_PROCS; other_pid += 1) {
//						if (other_pid == my_proc_id)
//							continue;
//
//						for (int idx = 0; idx < cross_partition_nodes[other_pid][my_proc_id][dir].size(); idx++) {
//							int node = cross_partition_nodes[other_pid][my_proc_id][dir][idx].first,
//								weight = cross_partition_nodes[other_pid][my_proc_id][dir][idx].second;
//
////							cout << PROC_HEADER << "Direction: " << dir << " Received Node: " << node << " Weight: " << weight << " from Proc: " << other_pid << endl;
//							if (node_weights[dir].find(node) == node_weights[dir].end()
//									|| node_weights[dir][node] > weight) {
//
////								cout << "\t" << PROC_HEADER << "Direction: " << dir << " Pushing Node: " << node << endl;
////								cout << "\t" << PROC_HEADER << "Direction: " << dir << " Queue before Push: " << pqueues[dir].get_dump() << endl;
//
//								node_weights[dir][node] = weight;
//								pqueues[dir].decrement_key(node);
//
////								cout << PROC_HEADER << "Direction: " << dir << " Queue after Push: " << pqueues[dir].get_dump() << endl;
//
//							}
//							else {
////								cout << "\t" << PROC_HEADER << "Direction: " << dir << " Skipped Node: " << node << endl;
//							}
//						}
//
//						cross_partition_nodes[other_pid][my_proc_id][dir].clear();
//					}
////					cout << PROC_HEADER << "Direction: " << dir << " Queue: " << pqueues[dir].get_dump() << endl;
//				}
//
//				queue_sizes[my_proc_id] = pqueues[FORWARD].get_size() + pqueues[BACKWARD].get_size();

//				/* Ensure everyone is complete processing their in-bound messages */
//				#pragma omp barrier

//				MPI_Barrier(MPI_COMM_WORLD); /* Sync up at the end of the iteration */
				if (my_proc_id == 0) {
					gettimeofday(&end, &tzp);
					elapsed_usecs_msg += get_elapsed(&start, &end);

//					cout << "Iteration: " << iter << " Min Distance: " << minimum_distance << " Queue Size: " << total_qsize << endl;
				}
			}
		}

		if (my_proc_id == 0) {
			elapsed_usecs_comp /= NUM_ITERS, elapsed_usecs_msg /= NUM_ITERS;
			cout << OUT_DEL << "MPI " << NUM_PROCS << " " << minimum_distance << " " << iter << " "
					<< elapsed_usecs_comp << " " << elapsed_usecs_msg << " " << (elapsed_usecs_comp + elapsed_usecs_msg) << endl;
		}

	}

	MPI_Finalize();
	return 0;

}
