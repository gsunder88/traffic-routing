/*
 * adjacency_splitter.cpp
 *
 *  Created on: May 2, 2015
 *      Author: ga-capar
 */


#include <fstream>
#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>

using namespace std;

int main(int argc, char **argv) {
	if (argc < 4) {
		cout << "Usage: ./binary <adjacency_file> <split-prefix> <num_procs>" << endl;
		return 0;
	}

	int num_procs = atoi(argv[3]);
	string prefix(argv[2]);
	string data[num_procs];

	ifstream infile(argv[1]);
	string line;
	int line_num = 1;
	while (getline(infile, line))
		data[line_num%num_procs] += line + "\n", line_num += 1;

	for (int p = 0; p < num_procs; p += 1) {
		stringstream SS;
		SS << prefix << "_" << num_procs << "_" << p;

		string outfile_name;
		SS >> outfile_name;
		ofstream outfile(outfile_name.c_str());
		outfile << data[p];
	}
}




