/*
 *  prefix_mpi.cpp - Computes the PREFIX SUMs using the Hypercube model on MPI.
 */

/*---------------------------------------------------------
 *  Parallel Prefix Sums
 *
 *  1. each processor generates numints/nprocs random integers (in parallel)
 *  2. each processor sums his numints/nprocs random integers (in parallel)
 *  3  Time for processor-wise prefix communications
 *  3.1  Each processor communicates with its pair at various levels from 0 to log(nprocs).
 *  3.2  At the end, using the prefix information of all previous procs, each prints out the sequential prefix sum.
 *  3.3  To ensure sequential I/O into the files, each processor expects a DUMMY message from its predecessor (if applicable)
 *       and sends a DUMMY message after it has done its I/O
 *  3.4  Finally, file "INPUT_MPI" has all the random ints in each processor one line at a time and "OUTPUT_MPI" contains
 *       corresponding prefix sums.
 *  4. It takes O(numints/nprocs + nprocs + log(nprocs)) time.
 *
 *  NOTE: steps 2-3 are repeated as many times as requested (numiterations) to get the average performance information.
 *---------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <mpi.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>

using namespace std;

/*==============================================================
 * p_generate_random_ints (processor-wise generation of random ints)
 *==============================================================*/
void p_generate_random_ints(vector<int>& memory, int n) {

  int i;

  /* generate & write this processor's random integers */
  for (i = 0; i < n; ++i) {

    memory.push_back(rand());
  }
  return;
}

/*
 * We ensure that the work is divided among all the processors equally, except the last one.
 * The last proc alone might get to work with a little more integers but the difference between
 * block and last_block below can be utmost num_threads which denotes the number of threads.
 * Since that is not a huge value, I have taken this approach. For example, when we split 11
 * integers among 3 processors, ideally we could split them as {4, 4, 3} but our allocation
 * would be {3, 3, 5} which again is not comparitively any worse given the fact that max-difference
 * of num_threads is never going beyond a couple of 100s in the most extreme case.
 *
 * Alternatively, I could have tried splitting it as {4, 4, 3} but doing that would complicate
 * computations unnecessarily in cases like num_threads = 5 and numints = 18 where {3, 3, 3, 3, 6}
 * is much easier since only the last proc is different whereas the ideal allocation {4, 4, 4, 3, 3}
 * would have more than 1 proc different than others.
 */
int get_block_size(int tid, int num_threads, int numints) {
  int block = numints/num_threads;
  int last_block = numints - block * (num_threads-1);

  return (tid == num_threads - 1) ? last_block : block;
}


// Functor to sum the numbers
struct sum_functor {

  // Constructor
  sum_functor() : m_sum(0) {
  }

  void operator()(int& num) {
    m_sum +=num;
  }

  long get_sum() const {
    return m_sum;
  }

  protected:

  long m_sum;
};

long p_summation(vector<int>& memory) {

  sum_functor result = std::for_each(memory.begin(), memory.end(), sum_functor());
  return result.get_sum();
}

/*==============================================================
 * print_elapsed (prints timing statistics) - ported from sample
 *==============================================================*/
 void print_elapsed(const char* desc, struct timeval* start, struct timeval* end, int niters) {

  struct timeval elapsed;
  /* calculate elapsed time */
  if(start->tv_usec > end->tv_usec) {

    end->tv_usec += 1000000;
    end->tv_sec--;
  }
  elapsed.tv_usec = end->tv_usec - start->tv_usec;
  elapsed.tv_sec  = end->tv_sec  - start->tv_sec;

  printf("\n %s total elapsed time = %ld (usec)\n",
    desc, (elapsed.tv_sec*1000000 + elapsed.tv_usec) / niters);
}

/*
 * Main Program (Parallel Prefix Sums)
 */
int main(int argc, char **argv) {

  int nprocs, numints, numiterations; /* command line args */

  int my_id, iteration;

  long running_sum; // Contains the total sum information from all communicating blocks
  long prefix_sum;  // Contains the sum info for all preceding blocks which in the end results in prefix-sums of all previous blocks

  vector<int> mymemory; /* Vector to store processes numbers        */
  long* buffer;         /* Buffer for inter-processor communication */

  struct timeval gen_start, gen_end; /* gettimeofday stuff */
  struct timeval start, end;         /* gettimeofday stuff */
  struct timezone tzp;

  char *inputf = "INPUT_MPI", *outputf = "OUTPUT_MPI";

  MPI_Status status;              /* Status variable for MPI operations */

  /*---------------------------------------------------------
   * Initializing the MPI environment
   * "nprocs" copies of this program will be initiated by MPI.
   * All the variables will be private, only the program owner could see its own variables
   * If there must be a inter-procesor communication, the communication must be
   * done explicitly using MPI communication functions.
   *---------------------------------------------------------*/

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_id); /* Getting the ID for this process */

  /*---------------------------------------------------------
   *  Read Command Line
   *  - check usage and parse args
   *---------------------------------------------------------*/

  if(argc < 3) {

    if(my_id == 0)
      printf("Usage: %s [numints] [numiterations]\n\n", argv[0]);

    MPI_Finalize();
    exit(1);
  }

  numints       = atoi(argv[1]);
  numiterations = atoi(argv[2]);

  MPI_Comm_size(MPI_COMM_WORLD, &nprocs); /* Get number of processors */

  if(my_id == 0)
    printf("\nExecuting %s: nprocs=%d, numints=%d, numiterations=%d\n",
            argv[0], nprocs, numints, numiterations);

  /*---------------------------------------------------------
   *  Initialization
   *  - allocate memory for work area structures and work area
   *---------------------------------------------------------*/

  // Get the number of ints to be handled by this specific processor
  numints = get_block_size(my_id, nprocs, numints);

  mymemory.reserve(numints);
  buffer = (long *) malloc(sizeof(long));

  if(buffer == NULL) {

    printf("Processor %d - unable to malloc()\n", my_id);
    MPI_Finalize();
    exit(1);
  }

  /* get starting time */
  gettimeofday(&gen_start, &tzp);
  srand(my_id + time(NULL));                  /* Seed rand functions */
  p_generate_random_ints(mymemory, numints);  /* random parallel fill */
  gettimeofday(&gen_end, &tzp);

  if(my_id == 0) {

    print_elapsed("Input generated", &gen_start, &gen_end, 1);

    /* Ensure the INPUT & OUTPUT files are empty.
     * This is to ensure we dont append to results from previous runs.
     */
    remove(inputf), remove(outputf);
  }

  MPI_Barrier(MPI_COMM_WORLD); /* Global barrier to begin processing*/
  gettimeofday(&start, &tzp);

  /* repeat for numiterations times */
  for (iteration = 0; iteration < numiterations; iteration++) {

    /* Compute the local summation */
    running_sum = p_summation(mymemory);
    prefix_sum = 0;

    /*
     * Hypercube model where each processor interfaces with different levels for log(nprocs) iterations -
     * Similar idea was used in Homework Assignment II
     */

    for (int bit = 0; (1 << bit) < nprocs; bit += 1) {
      int pair = my_id ^ (1 << bit);

      /* If the pair is beyond limits, we can skip the step */
      if (pair >= nprocs)
          continue;

      MPI_Send(&running_sum, 1, MPI_LONG, pair, 0, MPI_COMM_WORLD);
      MPI_Recv(buffer, 1, MPI_LONG, pair, 0, MPI_COMM_WORLD, &status);

      running_sum += *buffer;

      /* If the pair is before this block, its sum value is part of this block's prefix sum */
      if (pair < my_id)
          prefix_sum += *buffer;
    }
  }

  /* Proc 0 takes care of printing timing statistics */
  if(my_id == 0) {
    gettimeofday(&end,&tzp);
    print_elapsed("Prefix Summation", &start, &end, numiterations);
  }

  MPI_Barrier(MPI_COMM_WORLD); /* Global barrier to sync & initiate the I/O part */

  /*
   * This section sequentially outputs the random integers and the prefix sums from each block.
   * Each proc waits for a message from its predessor before I/O and then sends the same message
   * to its successor. The value sent along is just a DUMMY value.
   */

  /* If there is a predecessor, wait for a SIGNAL from it before I/O */
  if (my_id > 0)
    MPI_Recv(buffer, 1, MPI_LONG, my_id-1, 0, MPI_COMM_WORLD, &status);

  ofstream ihandle, ohandle;
  /* Open files in append mode since each proc just adds a new line */
  ihandle.open(inputf, ios::out | ios::app), ohandle.open(outputf, ios::out | ios::app);

  for (int i = 0; i < numints; i += 1) {
    ihandle << mymemory[i] << " ";
    ohandle << (mymemory[i] + prefix_sum) << " ";
    prefix_sum += mymemory[i];
  }

  ihandle << endl, ohandle << endl;
  ihandle.close(), ohandle.close();

  /* If there is a successor, send a DUMMY message to SIGNAL for I/O initiation */
  if (my_id < nprocs-1)
    MPI_Send(buffer, 1, MPI_LONG, my_id+1, 0, MPI_COMM_WORLD);


  /* free memory */
  free(buffer);

  MPI_Finalize();

  return 0;
} /* main() */
