'''
Created on Apr 23, 2015

@author: ga-capar
'''

from collections import defaultdict, deque

class Node:
    def __init__(self, id, lat, lon):
        self.id = id
        self.lat = lat
        self.lon = lon
        
class Edge:
    def __init__(self, id, start_node_id, end_node_id, dist):
        self.id = id
        self.start_node_id = start_node_id
        self.end_node_id = end_node_id
        self.dist = dist
        

def bfs(node, label, adjacency_list, node_labels, weighted):
    node_labels[node] = label
    queue = deque([node])
    count = 0
    while len(queue) > 0:
        current = queue.popleft()
        count += 1
        if count % 100000 == 0:
            print 'Completed processing %d nodes under label: %d' % (count, label)
        
        if current not in adjacency_list:
            continue
        
        neighbours = adjacency_list[current]
        if weighted is True:
            neighbours = [nei for (nei, weight) in neighbours] 
            
        for nei in neighbours:
            if node_labels[nei] == 0:
                node_labels[nei] = label
                queue.append(nei)
                
    print 'Label: %d has %d nodes in it' % (label, count) 
    return count   

def label_nodes(adjacency_list, weighted=False):
    node_labels = defaultdict(int)
    largest_component_id, largest_component_size = 0, 0
    label = 1
    for node in adjacency_list:
        if node_labels[node] == 0:
            sz = bfs(node, label, adjacency_list, node_labels, weighted)
            if sz > largest_component_size:
                largest_component_size = sz
                largest_component_id = label
            label += 1
            
    cnt = defaultdict(int)
    for lab in node_labels.values():
        cnt[lab] += 1    
        
    print 'Largest connected component has %d nodes' % (max(cnt.values()))
    return node_labels